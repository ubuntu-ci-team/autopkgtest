#!/usr/bin/perl
# Copyright 2006-2016 Canonical Ltd.
# Copyright 2023 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

# Usage: parse-deps.pl DEPS ARCH
# Parse DEPS as a d/control Depends string, and print dependencies suitable for
# DEBIAN/control Depends to stdout.
# Example:
# ./parse-deps.pl "a (>= 1), b [i386], c [amd64], d <!nocheck>, e:arm64" i386
# ->
# a (>= 1), b, d, e:arm64

use strict;
use warnings;

use Dpkg::Deps;

my $origdeps = shift;
my $arch = shift;

my $dep = deps_parse($origdeps, reduce_restrictions => 1, host_arch => $arch);
my $out = $dep->output();

print $out, "\n";
